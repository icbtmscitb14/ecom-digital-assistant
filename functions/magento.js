const config = require('./config');
const helpers = require('./helpers');
const rp = require('request-promise');

let customerUsername = 'gpnalin@gmail.com';
let customerPassword = 'Nets@123';

let currentCustomerToken;

/**
 * Returns customer token
 */
const getCustomerToken = async () => {
  console.log(`cusotmer token request api call triggered. requesting token for ${customerUsername}`);
  return await rp({
    method: 'POST',
    uri: helpers.getApiEndpoint('/rest/default/V1/integration/customer/token'),
    body: { "username": customerUsername, "password": customerPassword },
    headers: helpers.getHttpHeaders(config.magento_access_token),
    json: true
  });
}

/**
 * Returns currently logged in customer token if not set it will set it as well.
 */
const getCurrentCustomerToken = async () => {
  if (!currentCustomerToken) {
    currentCustomerToken = await getCustomerToken();
  }
  return currentCustomerToken;
}

/**
 * Returns current customer's active quote id
 */
const getCustomerQuoteId = async () => {
  const currentCustomerToken = await getCurrentCustomerToken();
  console.log(`requesting quote id for the for token:${currentCustomerToken}`);
  return await rp({
    method: 'POST',
    uri: helpers.getApiEndpoint('/rest/default/V1/carts/mine'),
    headers: helpers.getHttpHeaders(currentCustomerToken),
    json: true
  });
}

/**
 * Get items in the magento cart
 */
const getItemsInCart = async () => {
  try {
    const currentCustomerToken = await getCurrentCustomerToken();
    const currentCustomerQuoteId = await getCustomerQuoteId();
    let cartData = await rp({
      method: 'GET',
      uri: helpers.getApiEndpoint('/rest/default/V1/carts/mine/items'),
      headers: helpers.getHttpHeaders(currentCustomerToken),
      json: true
    });
    return cartData;
  } catch (error) {
    console.log(error);
    return false;
  }
}

/**
 * Check if the cart has items in it
 */
const hasItemsInCart = async () => {
  const itemsInCart = await getItemsInCart();
  return itemsInCart.length >= 1 ? true : false;
}

/**
 * Add product to the current logged in customer's cart
 * @param {*} productSku 
 * @param {*} qty 
 */
const addProductToCustomerCart = async (productSku, qty) => {
  try {
    const currentCustomerToken = await getCurrentCustomerToken();
    const currentCustomerQuoteId = await getCustomerQuoteId();
    console.log(`adding ${productSku} (qty:${qty}) to ${customerUsername} customer's cart(${currentCustomerQuoteId})`);
    let cartData = await rp({
      method: 'POST',
      uri: helpers.getApiEndpoint('/rest/default/V1/carts/mine/items'),
      headers: helpers.getHttpHeaders(currentCustomerToken),
      body: { "cartItem": { "sku": productSku, "qty": qty, "quote_id": currentCustomerQuoteId } },
      json: true
    });
    console.log(`${productSku} added successfully to cart `, cartData);
    return cartData;
  } catch (error) {
    console.log(error);
    return false;
  }
}

/**
 * Returns customer shipping address data formatted
 */
const getCustomerShippingAddress = async () => {
  const currentCustomerToken = await getCurrentCustomerToken();
  const customerAddress = await rp({
    method: 'GET',
    uri: helpers.getApiEndpoint('/rest/default/V1/customers/me/shippingAddress'),
    headers: helpers.getHttpHeaders(currentCustomerToken),
    json: true
  });
  return {
    "region": customerAddress.region.region,
    "region_id": customerAddress.region_id,
    "region_code": customerAddress.region.region_code,
    "country_id": customerAddress.country_id,
    "street": customerAddress.street,
    "postcode": customerAddress.postcode,
    "city": customerAddress.city,
    "firstname": customerAddress.firstname,
    "lastname": customerAddress.lastname,
    "customer_id": customerAddress.customer_id,
    "telephone": customerAddress.telephone,
    "same_as_billing": 1

  };
}

/**
 * Returns customer shipping address
 * @param {*} customerShippingAddress 
 */
const estimateShippingCosts = async (customerShippingAddress) => {
  const currentCustomerToken = await getCurrentCustomerToken();
  try {
    let shippingMethods = await rp({
      method: 'POST',
      uri: helpers.getApiEndpoint('/rest/default/V1/carts/mine/estimate-shipping-methods'),
      headers: helpers.getHttpHeaders(currentCustomerToken),
      body: { "address": customerShippingAddress },
      json: true
    });
    return shippingMethods;
  } catch (error) {
    console.log(error);
    return false;
  }
}

/**
 * Set customer shipping address to the quote
 * @param {*} customerShippingAddress 
 * @param {*} selectedShippingMethod 
 */
const setShippingAndBilling = async (customerShippingAddress, selectedShippingMethod) => {
  const currentCustomerToken = await getCurrentCustomerToken();
  return await rp({
    method: 'POST',
    uri: helpers.getApiEndpoint('/rest/default/V1/carts/mine/shipping-information'),
    headers: helpers.getHttpHeaders(currentCustomerToken),
    body: {
      "addressInformation": {
        "shipping_address": customerShippingAddress,
        "billing_address": customerShippingAddress,
        "shipping_carrier_code": selectedShippingMethod.carrier_code,
        "shipping_method_code": selectedShippingMethod.method_code
      }
    },
    json: true
  });
}

/**
 * Get available payment methods for the customer
 * @param {*} customerShippingAddress 
 * @param {*} selectedShippingMethod 
 */
const getAvailablePaymentMethods = async (customerShippingAddress, selectedShippingMethod) => {
  const currentCustomerToken = await getCurrentCustomerToken();
  return await rp({
    method: 'GET',
    uri: helpers.getApiEndpoint('/rest/default/V1/carts/mine/payment-information'),
    headers: helpers.getHttpHeaders(currentCustomerToken),
    json: true
  });
}

/**
 * Send Payment and it will create the order.
 * @param {*} customerShippingAddress 
 * @param {*} selectedShippingMethod 
 * @param {*} selectedPaymentMethod 
 */
const sendPayment = async (customerShippingAddress, selectedShippingMethod, selectedPaymentMethod) => {
  const currentCustomerToken = await getCurrentCustomerToken();
  return await rp({
    method: 'POST',
    uri: helpers.getApiEndpoint('/rest/default/V1/carts/mine/payment-information'),
    headers: helpers.getHttpHeaders(currentCustomerToken),
    body: {
      "paymentMethod": {
        "method": selectedPaymentMethod.code
      },
      "shipping_address": customerShippingAddress,
      "billing_address": customerShippingAddress,
      "shipping_carrier_code": selectedShippingMethod.carrier_code,
      "shipping_method_code": selectedShippingMethod.method_code
    },
    json: true
  });
}


/**
 * Returns customer token
 */
const getOrderDetails = async (orderId) => {
  return await rp({
    method: 'GET',
    uri: helpers.getApiEndpoint(`/rest/default/V1/orders/${orderId}`),
    headers: helpers.getHttpHeaders(config.magento_access_token),
    json: true
  });
}

const productSearch = async queryString => {
  try {
    const products = await rp({
      method: 'GET',
      uri: helpers.getApiEndpoint('/rest/V1/products'),
      headers: helpers.getHttpHeaders(config.magento_access_token),
      qs: queryString,
      json: true
    });
    console.log('product search results', products);
    return products;
  } catch (error) {
    console.error('product search failed', error);
    return false;
  }
}

const formatProductsData = products => {
  if (products.length === 0) return false;
  return products.map(product => ({
    name: product.name,
    sku: product.sku,
    price: `${product.price} USD`,
    url: `${config.magento_base_url}/${product.custom_attributes.find(item => item.attribute_code === 'url_key').value}.html`,
    description: product.custom_attributes.find(item => item.attribute_code === 'description').value,
    image: {
      url: `${config.magento_base_url}/pub/media/catalog/product${product.custom_attributes.find(item => item.attribute_code === 'thumbnail').value}`,
      alt: product.name
    }
  }));
}

/**
 * Get product data by using product sku
 * @param {*} productSku 
 */
const getProductBySku = async (productSku) => {
  try {
    let product = await rp({
      method: 'GET',
      uri: helpers.getApiEndpoint(`/rest/default/V1/products/${productSku}`),
      headers: helpers.getHttpHeaders(config.magento_access_token),
      // qs: {fields:'id,sku,name'},
      json: true
    });
    console.log(`getProductBySku----`, product);
    return formatProductsData([product]);
  } catch (error) {
    return false;
  }

}

/**
 * Remove the cart item using sku
 * @param {*} productSku
 */
const removeProductFromCartBySku = async (productSku) => {
  const currentCustomerToken = await getCurrentCustomerToken();
  const cartProducts = await getItemsInCart();
  const hasProductInCart = cartProducts.find(cartProduct => cartProduct.sku === productSku);
  if (hasProductInCart !== undefined) {
    try {
      console.log(`removing cart item: ${hasProductInCart.item_id}`);
      return await rp({
        method: 'DELETE',
        uri: helpers.getApiEndpoint(`/rest/default/V1/carts/mine/items/${hasProductInCart.item_id}`),
        headers: helpers.getHttpHeaders(currentCustomerToken),
        json: true
      });
    } catch (error) {
      console.error(error);
      return false;
    }

  }
  return false;
}

module.exports = {
  customerUsername: customerUsername,
  customerPassword: customerPassword,
  currentCustomerToken: currentCustomerToken,
  getCustomerToken,
  getCurrentCustomerToken,
  getCustomerQuoteId,
  getItemsInCart,
  hasItemsInCart,
  addProductToCustomerCart,
  getCustomerShippingAddress,
  estimateShippingCosts,
  setShippingAndBilling,
  getAvailablePaymentMethods,
  sendPayment,
  productSearch,
  getProductBySku,
  removeProductFromCartBySku,
  getOrderDetails,
  formatProductsData,
};

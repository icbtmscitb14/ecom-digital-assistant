'use strict';

const { dialogflow, Suggestions, List, Image } = require('actions-on-google');
const functions = require('firebase-functions');
const config = require('./config');
const magento = require('./magento');

const dialogflow2 = require('dialogflow');
const sessionEntityTypesClient = new dialogflow2.SessionEntityTypesClient();

const app = dialogflow({ debug: true });

app.middleware((conv) => {
  conv.hasScreen =
      conv.surface.capabilities.has('actions.capability.SCREEN_OUTPUT');
  conv.hasAudioPlayback =
      conv.surface.capabilities.has('actions.capability.AUDIO_OUTPUT');
});

app.intent('Default Welcome Intent', (conv) => {
  conv.add(`Welcome to ${config.agent_name}! Would you like to buy anything today?`);
  conv.add(new Suggestions([`Yes`, `No`]));
});
app.intent('Default Welcome Intent - no', (conv) => {
  conv.close(`Thank you for using ICBT Shop. See you again.`);
});
app.intent('Default Welcome Intent - yes', (conv) => {
  conv.add(`Welcome to Rainco Umbrella Store Digital Shopping Assistant. We have ladies, gents, children's umbrellas and umbrellas for special occasions. What type of umbrella are you looking for?`);
  conv.add(new Suggestions([`Show products`]));
});
app.intent('Default Fallback Intent', (conv) => {
  conv.add(`I didn't understand`);
  conv.add(`I'm sorry, can you try again?`);
  conv.add(new Suggestions([`Show products`]));
});

app.intent('product.search', async (conv) => {
  console.log('product.search params:::: ', conv.parameters);
  conv.user.storage.searchTerm = conv.parameters.any;
  conv.user.storage.product_category = conv.parameters.categories;
  conv.user.storage.pageSize = 1;
  conv.user.storage.currentPage = 1;

  let products = await queryProducts(conv);
});


app.intent('product.search - next', async conv => {
  conv.user.storage.currentPage = conv.user.storage.currentPage + 1;
  let products = await queryProducts(conv);

});

app.intent('product.search - previous', async conv => {
  conv.user.storage.currentPage = conv.user.storage.currentPage - 1;
  let products = await queryProducts(conv);
});

const queryProducts = async conv => {
  let qs = {
    "searchCriteria": {
      "filterGroups": [
        { "filters": [{ "field": "status", "value": 1, "condition_type": "eq" }] },
        { "filters": [{ "field": "visibility", "value": 4, "condition_type": "eq" }] }
      ],
      "pageSize": conv.user.storage.pageSize,
      "currentPage": conv.user.storage.currentPage,
    }
  };
  if (conv.user.storage.product_category) {
    console.log(`conv.user.storage.product_category`, conv.user.storage.product_category);
    qs.searchCriteria.filterGroups[{ "filters": [{ "field": "category_id", "value": conv.user.storage.product_category, "condition_type": "eq" }] }];
  } else if (conv.user.storage.searchTerm) {
    console.log(`conv.user.storage.searchTerm`, conv.user.storage.searchTerm);
    qs.searchCriteria.filterGroups[{ "filters": [{ "field": "name", "value": `%${conv.user.storage.searchTerm}%`, "condition_type": "like" }] }];
  }

  let result = await magento.productSearch(qs);
  let products = magento.formatProductsData(result.items);

  console.log(`search result: total_count:${result.total_count} page_size:${result.search_criteria.page_size}, current_page:${result.search_criteria.current_page}`,result);
  /**
   * Update products entity with latest found products
   */
  await updateProductsEntity(conv.body.session, products);

  if (products.length === 0) {
    conv.add(`Sorry. We coudn't find products for your query`);
    return;
  }
  conv.user.storage.currentProduct = { sku: products[0].sku, qty: 1 };
  let response_str ='';
  let product_str = products.map(product => product.name).join(', ');
  if (conv.user.storage.searchTerm !== undefined && conv.user.storage.searchTerm === ""){
    if (conv.user.storage.currentPage ===1) response_str += `We found ${result.total_count} products for ${conv.user.storage.searchTerm}.`;
    response_str += `${product_str} is the ${result.search_criteria.current_page} of ${result.total_count}.`;
  }
  conv.add(response_str);
  if (conv.user.storage.currentPage ===1){
    conv.add(`You can navigate through the products using by using next/prev.`);
  }
  conv.add(new Suggestions([`Next`,`Prev`,`Add this to cart`]));
};

const updateProductsEntity = async (session, products) => {

  if (products.length === 0) return;

  const entityTypeDisplayName = 'products';
  const sessionId = session.split('/').splice(-1)[0];

  const productSessionEntities = products.map(product => ({ value: product.sku, synonyms: [product.sku, product.name] }));

  const sessionEntityTypePath = sessionEntityTypesClient.sessionEntityTypePath(
      config.project_id,
      sessionId,
      entityTypeDisplayName
  );

  const sessionEntityTypeRequest = {
    parent: session,
    sessionEntityType: {
      name: sessionEntityTypePath,
      entityOverrideMode: 'ENTITY_OVERRIDE_MODE_SUPPLEMENT',
      entities: productSessionEntities
    }
  };

  try {
    await sessionEntityTypesClient.createSessionEntityType(sessionEntityTypeRequest);
  } catch (error) {
    console.error('Error creating session entitytype: ', error);
  }
}

app.intent('actions_intent_OPTION', async conv => {
  conv.add(`actions_intent_OPTION intent triggered`);
});

app.intent('product.search - select', async conv => {
  let product_sku = conv.parameters.products;
  console.log(`product_sku::: `, product_sku);
  let products = await magento.getProductBySku(product_sku);
  console.log(`products::: `, products);
  conv.add(`${products[0].name} is costs ${products[0].price}`);
});
// Get the Confirmation from the user to add the product into the CART
app.intent('product.add-to-cart', (conv) => {
  console.log(conv.parameters);
  let products = conv.parameters.products ? conv.parameters.products :  conv.user.storage.currentProduct.sku ;
  let qty = conv.parameters.qty;
  conv.contexts.set('productadd-to-cart-followup', 5, { products: products, qty: qty });
  conv.add(`Is that correct that you want to add ${products} to the cart?`);
  conv.add(new Suggestions([`Yes`, `No`]));
});

// -----------------Add the product into the CART - YES------------------
app.intent('product.add-to-cart - yes', async (conv) => {

  const context = conv.contexts.get('productadd-to-cart-followup');

  const productSku = context.parameters.products;
  const productQty = context.parameters.qty;

  let cart = await magento.addProductToCustomerCart(productSku, productQty);
  if (cart) {
    conv.add(`${productSku} was successfully added to the cart. Would you like to checkout or continue shopping?`);
    conv.add(new Suggestions([`Continue Shopping`, `Checkout`]));
  } else {
    conv.add(`Sorry. We were unable to add ${productSku} to your cart. Could you please try again!`);
  }
});

// ----------Continue Shopping-------------- 
app.intent('continue-shopping', async (conv) => {
  // const context = conv.contexts.get('productadd-to-cart-followup');
  conv.ask(`You can check catalog by saying 'show products'`);
  conv.add(new Suggestions([`Show products`]));
});

// -----------------Add the product into the CART - NO------------------
app.intent('product.add-to-cart - no', async (conv) => {
  const context = conv.contexts.get('productadd-to-cart-followup');
  conv.ask('Would you like to keep shopping or stop here?');
    conv.add(new Suggestions([`Continue Shopping`, `Stop Here`]));
});

// -----------------Add the product into the CART - NO -Exit) ------- 
app.intent('product.add-to-cart - no - custom', async (conv) => { 
  conv.close(`Okay, let's try this again later.`);
});

//  ------- 1. Starting product checkout process ------- 
app.intent('product-checkout', async (conv) => {

  let hasCartItems = await magento.hasItemsInCart();
  const cartItemslist = await magento.getItemsInCart();
  const cartItemsNamesList = cartItemslist.map(item => `${item.name} (${item.qty})`);
  let cartItemsNames = cartItemsNamesList.join();

  const customerShippingAddress = await magento.getCustomerShippingAddress();

  let customerShippingAdd = `${customerShippingAddress.firstname} ${customerShippingAddress.lastname},${customerShippingAddress.street}, ${customerShippingAddress.city}, ${customerShippingAddress.postcode}`;

  const availablePaymentMethods = await magento.getAvailablePaymentMethods();
  const fullReviewCart = availablePaymentMethods.totals.items;
  const cartReview = fullReviewCart.map(item => `${item.name} (${item.qty})`);
  const grand_total_price = `${availablePaymentMethods.totals.grand_total} ${availablePaymentMethods.totals.base_currency_code}`;

  conv.contexts.set('product-checkout-followup', 5, { fullCart: cartReview, total_price: grand_total_price, customerShippingAddress: customerShippingAdd });
  // Check the avilability of iems in the cart
  if (hasCartItems) {
    conv.ask(`${cartItemsNames} are in your cart. Total order cost is ${grand_total_price}. Would you like to checkout?`);
    conv.add(new Suggestions([`Yes`, `No`]));
  }
  else {
    conv.add(`No items in your cart. Could you please add products to cart before checkout`);
  }

});


//  ------- 2. Starting product checkout process (Do you need to continue with checkout.? YES) ------- 
app.intent('product-checkout - yes', async (conv) => {
  const context = conv.contexts.get('product-checkout-followup');
  const customerShippingAddress = context.parameters.customerShippingAddress;

  let customerShippingAdd = await magento.getCustomerShippingAddress();
  const shippingMethods = await magento.estimateShippingCosts(customerShippingAdd);

  const shippingMethodsNameList = shippingMethods.map(item => item.carrier_title);
  const shippingMethodsList = shippingMethods.map(item => `${item.carrier_title} with $${item.amount} extra price.`);
  let shippingMethodsL = shippingMethodsList.join();

  conv.contexts.set('product-checkout-yes-followup', 5, { shippingMethodsL: shippingMethodsL, shippingMethodsArr: shippingMethodsNameList, customerShippingAddress: customerShippingAddress });

  conv.contexts.set(' checkout-context ', 50, { shippingAddress: customerShippingAdd });

  conv.ask(`Shipping address for your order is ${customerShippingAddress}. Can we proceed?`);
  conv.add(new Suggestions([`Edit`, `Proceed`]));

});
//  ------- 2. Starting product checkout process (Do you need to continue with checkout.? NO) ------- 
app.intent('product-checkout - no', async (conv) => {
  const context = conv.contexts.get('product-checkout-followup');
  conv.ask(`Do you need to add more items or need to exit from this service.`);
  conv.add(new Suggestions([`Add More`, `Exit`]));
});

// ------- 2. Checkout process (Do you need to continue with checkout.? NO -Exit) ------- 
app.intent('product-checkout - no - custom', async (conv) => { 
  conv.close(`Okay, let's try this again later.`);
});


// ------- 3. Starting product checkout process (Can we proceed.? Yes/proceed) ---------
app.intent('product-checkout - yes - custom', async (conv) => {
  const context = conv.contexts.get('product-checkout-yes-followup');
  const customerShippingAddress = context.parameters.customerShippingAddress;
  const shippingMethodsL = context.parameters.shippingMethodsL;
  const shippingMethodsArr = context.parameters.shippingMethodsArr;

  const context_1 = conv.contexts.get('checkout-context');
  const customerShipAddress = context_1.parameters.shippingAddress;

  // Globally save the shipping address in the flow
  conv.user.storage.address = customerShipAddress;

  const availablePaymentMethods = await magento.getAvailablePaymentMethods();
  const availablePaymentMethod = availablePaymentMethods.payment_methods;
  const payment_methods_List = availablePaymentMethod.map(item => item.title);
  let payment_methods = payment_methods_List.join();
  conv.contexts.set('product-checkout-yes-custom-followup', 5, { payment_methods_List: payment_methods, payment_methodsArr: payment_methods_List });

  conv.ask(`Your order can be shipped via ${shippingMethodsL}. Which Shipping method would you like?`);
  conv.add(new Suggestions(shippingMethodsArr));

});
// ----- 4. Starting product checkout process (Which Shipping method do you want to continue.? Any Answer)--------
app.intent('product-checkout - yes - custom - custom', async (conv) => {

  const selected_shipping_method = conv.parameters.shipping_way;
  const shippingMethods = await magento.estimateShippingCosts(conv.user.storage.address);
  console.log("shippingMethods----", shippingMethods);

  let selected_ship_method = shippingMethods.find((key) => {
    return key.carrier_code === conv.parameters.shipping_way;
  });

  conv.user.storage.selected_ship_method = selected_ship_method;

  const context = conv.contexts.get('product-checkout-yes-custom-followup');

  const payment_methods_List = context.parameters.payment_methods_List;
  const payment_methodsArr = context.parameters.payment_methodsArr;

  conv.contexts.set('product-checkout-yes-custom-custom-followup', 5, { payment_methods_List: payment_methods_List, payment_methodsArr: payment_methodsArr });
  conv.add(`You can pay with ${payment_methods_List}. Which one would you choose?`);
  conv.add(new Suggestions(payment_methodsArr));

});
// product-checkout - yes - custom - custom - custom
app.intent('product-checkout - yes - custom - custom - custom', async (conv) => {
  const context = conv.contexts.get('product-checkout-yes-custom-custom-followup');

  const availablePaymentMethods = await magento.getAvailablePaymentMethods();
  const availablePaymentMethod = availablePaymentMethods.payment_methods;

  // Filtering payment method object using the selected_payment_method
  let selected_pay_method = availablePaymentMethod.find((key) => {
    return key.code === conv.parameters.payment_way;
  });

  conv.user.storage.selected_pay_method = selected_pay_method;

  //  Console for all necessary items to the payment API
  console.log("USER ADDRESS", conv.user.storage.address);
  console.log("USER SHIPPING METHOD", conv.user.storage.selected_ship_method);
  console.log("USER PAYMENT METHOD", conv.user.storage.selected_pay_method);

  const a = conv.user.storage.address;
  const b = conv.user.storage.selected_ship_method;
  const c = conv.user.storage.selected_pay_method;

  const dataShipping = await magento.setShippingAndBilling(a, b);
  const data = await magento.sendPayment(a, b, c);
  const order_Id = await magento.getOrderDetails(data);

  const orderID = order_Id.increment_id;
  conv.close(`Thanks for shopping with ICBT Shop. Your order has been placed successfully and the order number is ${orderID}. Please check your email for more information.`);

  conv.user.storage.orderID = orderID;


});

exports.dialogflowFirebaseFulfillment = functions.https.onRequest(app);



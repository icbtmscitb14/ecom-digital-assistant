const config = require('./config');

const getHttpHeaders = (token) => ({
  'Authorization': 'Bearer ' + token,
  'Content-type': 'application/json',
  'Accept': 'application/json'
});
const getApiEndpoint = (endpoint) => config.magento_base_url + endpoint;

module.exports = {
  getHttpHeaders,
  getApiEndpoint
};